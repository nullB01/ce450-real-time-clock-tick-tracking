#include <iostream>
#include <thread>

#include "Service.hpp"
#include "../Semaphore/Semaphore.hpp"

void Service1(Semaphore &Sem);

bool g_Flag = true;

//Main is the caller 
int main()
{
    Service s1(Service1, 0, 2, 10, 10);

    s1.AuxBlock();

    for(int i = 0; i < 3; i++)
    {
        s1.Unblock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    
    g_Flag = false;
    s1.Unblock();
    return 0;
}

void Service1(Semaphore &Sem)
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
    Sem.Post_Caller();

    while(g_Flag)
    {
        Sem.Wait_Callee();
        std::cout<<"Task Dispatched"<<std::endl;
    }
    
}