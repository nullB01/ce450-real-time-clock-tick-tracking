#include "Service.hpp"

Service::Service(void (*function)(), 
        int priorityReduction, 
        int coreNum, 
        int servPeriod, 
        int servDeadline):
p_ServiceFunction(function),
m_Core(coreNum),
m_Period(servPeriod),
m_Deadline(servDeadline),
m_PriorityReduction(priorityReduction)
{
    p_ServiceFunctionSem = nullptr;
    p_Thread = MakeRealTimeThread();
    m_Info.Deadline = m_Deadline;
    m_Info.Period = m_Deadline;

    if(!VerifyThread())
    {
        std::cout<<"Unable to make configure thread exiting...";
        exit(1);
    }
}

Service::Service(void (*function)(Semaphore &, Clock &), 
        int priorityReduction, 
        int coreNum, 
        int servPeriod, 
        int servDeadline,
        Clock &clock):
p_ServiceFunctionSem(function),
m_Core(coreNum),
m_Period(servPeriod),
m_Deadline(servDeadline),
m_PriorityReduction(priorityReduction)
{
    p_ServiceFunction = nullptr;
    p_Thread = MakeRealTimeThread(clock);

    m_Info.Deadline = m_Deadline;
    m_Info.Period = m_Period;

    if(!VerifyThread())
    {
        std::cout<<"Unable to make configure thread exiting...";
        exit(1);
    }
}

void Service::Unblock()
{
    m_Sem.Post_Callee();
}

void Service::Block()
{
    m_Sem.Wait_Callee();
}

void Service::AuxBlock()
{
    m_Sem.Wait_Caller();
}

void Service::AuxUnblock()
{
    m_Sem.Post_Caller();
}

std::thread* Service::GetThread()
{
    return p_Thread;
}

Service::ServiceInfo Service::GetInfo()
{
    return m_Info;
}

std::thread* Service::MakeRealTimeThread()
{
    int rc, maxprio;
    struct sched_param threadParams;
    cpu_set_t threadCPU;
    std::thread *newService;

    newService = new std::thread(p_ServiceFunction);
    
    CPU_ZERO(&threadCPU);
    CPU_SET(m_Core, &threadCPU);

    maxprio = sched_get_priority_max(SCHED_FIFO);
    threadParams.sched_priority = maxprio - m_PriorityReduction;

    rc = pthread_setschedparam(newService->native_handle(), SCHED_FIFO, &threadParams);
    rc = pthread_setaffinity_np(newService->native_handle(), sizeof(cpu_set_t), &threadCPU);
    rc = pthread_setschedprio(newService->native_handle(), threadParams.sched_priority);

    return newService;
}

std::thread* Service::MakeRealTimeThread(Clock &clock)
{
    int rc, maxprio;
    struct sched_param threadParams;
    cpu_set_t threadCPU;
    std::thread *newService;

    newService = new std::thread(p_ServiceFunctionSem, std::ref(m_Sem), std::ref(clock));
    
    CPU_ZERO(&threadCPU);
    CPU_SET(m_Core, &threadCPU);

    maxprio = sched_get_priority_max(SCHED_FIFO);
    threadParams.sched_priority = maxprio - m_PriorityReduction;

    rc = pthread_setschedparam(newService->native_handle(), SCHED_FIFO, &threadParams);
    rc = pthread_setaffinity_np(newService->native_handle(), sizeof(cpu_set_t), &threadCPU);
    rc = pthread_setschedprio(newService->native_handle(), threadParams.sched_priority);

    return newService;
}

bool Service::VerifyThread()
{
    int rc;
    int policy;
    int maxprio;
    struct sched_param threadParams;
    bool result = true;

    rc = pthread_getschedparam(p_Thread->native_handle(), &policy, &threadParams);
    maxprio = sched_get_priority_max(SCHED_FIFO);

    if(policy != SCHED_FIFO)
    {
        result = false;
    }

    if(threadParams.sched_priority != (maxprio - m_PriorityReduction))
    {
        result = false;
    }

    return result;
}