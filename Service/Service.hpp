/*
* Service.hpp
* Providing framework for creating a service / task
* Kevin Pereira
*/

#ifndef SERVICE_HPP
#define SERVICE_HPP

#define _GNU_SOURCE

#include <thread>
#include <functional>
#include <iostream>

//#include <sched.h>


#include "../Semaphore/Semaphore.hpp"
#include "../Clock/Clock.hpp"

class clock;

class Service
{
public:
    typedef struct ServiceInfo
    {
        int Period, Deadline;
    }ServiceInfo;


    Service(void (*function)(), 
            int priorityReduction, 
            int coreNum, 
            int servPeriod, 
            int servDeadline);

    Service(void (*function)(Semaphore &, Clock &), 
            int priorityReduction, 
            int coreNum, 
            int servPeriod, 
            int servDeadline,
            Clock &clock); //may have to add a template for varying inputs if one exists. May be not depends on how architecture is designed
    
    void Unblock();
    void Block();
    void AuxBlock();
    void AuxUnblock();
    std::thread* GetThread();
    ServiceInfo GetInfo();

private:
    Service();
    std::thread *MakeRealTimeThread(Clock &clock);
    std::thread *MakeRealTimeThread();
    bool VerifyThread();

    typedef void (*FunctionPointerSem)(Semaphore &, Clock &);
    typedef void (*FuncitonPointer)();

protected:
    int m_Core, 
        m_Period,
        m_Deadline,
        m_PriorityReduction;

    ServiceInfo m_Info;
    
    std::thread *p_Thread;

    Semaphore m_Sem;

    FunctionPointerSem p_ServiceFunctionSem;
    FuncitonPointer p_ServiceFunction;
};

#endif //SERVICE_HPP