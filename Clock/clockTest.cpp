#include <chrono>
#include <thread>

#include "Clock.hpp"

void test();

int main()
{
std::string token = "CLOCKTEST:"; 
  Clock *pinstance = new Clock(NANOSECOND_PER_SECOND);


   pinstance->Start();
   std::string start("Start");
   pinstance->LogEvent(start, token);

   pinstance->Tick();

   std::string end("End");
   pinstance->LogEvent(end, token);

   pinstance->Adjust(1);

   std::string token1 = "CLOCKTEST:"; 
   Clock test(NANOSECOND_PER_SECOND);

   test.Start();
   std::string start1("Start");
   test.LogEvent(start1, token1);

   test.Tick();

   std::string end1("End");
   test.LogEvent(end1, token1);

   test.Adjust(1);

   return 0;
   
}


void test()
{
   
}