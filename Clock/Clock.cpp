/*
 * Clock.cpp 
 * Providing high resolution and monotomic timing resources.
 * Kevin Pereira
 */

#include "Clock.hpp"

Clock::Clock(uint32_t period, uint32_t adjust):
m_Tick(period),
m_Adjustment(adjust)
{
    m_TickAdjusted = m_Tick;
}

double Clock::Duration()
{
    return (std::chrono::steady_clock::now() - this->m_StartTime).count();
}

void Clock::Start()
{ 
    std::string msg("Start: "), token("CLOCK");
    this->m_StartTime = std::chrono::steady_clock::now();
    LogEvent(msg, token);
}

void Clock::LogEvent(std::string &input, std::string &token)
{
    double lapse = Duration()/NANOSECOND_PER_SECOND;
    const std::string marker = "REALTIME--",
                    timeHeader = ",TIME:";
    std::ostringstream os;

    os<<marker<<token<<' '<<std::fixed<<timeHeader<<','<<lapse<<','<<"Message,"<<input<<std::endl;
    
    syslog(LOG_CRIT, os.str().c_str());
}

void Clock::Tick()
{
    std::this_thread::__sleep_for(std::chrono::seconds(0), this->m_TickAdjusted);
}

void Clock::Adjust(uint64_t tick_num)
{
    std::ostringstream os;
    std::string token = "ADJUSTMENT";
    std::string msg;
    double duration = Duration() - (double)(tick_num * m_Tick.count()) / (double)NANOSECOND_PER_SECOND;
    duration /= tick_num;
    os << "REALTIME -- Duration expected,"<<std::fixed<<m_Tick.count()/(double)NANOSECOND_PER_SECOND<<','<<" Achieved,"<<std::fixed<<duration/(double)NANOSECOND_PER_SECOND << ",time ";

    if(duration < m_Tick.count())
    {
        m_TickAdjusted += m_Adjustment;
        os<<"increased"<<std::endl;
    }
    else if(duration > m_Tick.count())
    {
        m_TickAdjusted -= m_Adjustment;
        os<<"decreased"<<std::endl;
    }

    msg = os.str();
    LogEvent(msg, token);
}

double Clock::Duration_Seconds()
{
    return Duration() / (double) NANOSECOND_PER_SECOND;
}

void Clock::ServiceStart()
{
    m_ServiceStart = std::chrono::steady_clock::now();
}

void Clock::ServiceEnd(std::string &serviceName)
{
    std::ostringstream os;
    std::string msg("Execution time,");

    os<<msg <<std::fixed<< (std::chrono::steady_clock::now() - m_ServiceStart).count() / (double)NANOSECOND_PER_SECOND \
      <<',';
    msg = os.str();
    LogEvent(msg, serviceName);
}