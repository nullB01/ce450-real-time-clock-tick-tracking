/*
 * Clock.hpp 
 * Providing high resolution and monotomic timing resources.
 * Kevin Pereira
 */

#ifndef CLOCK_HPP
#define CLOCK_HPP

#include <iostream>
#include <chrono>
#include <string>
#include <sstream>
#include <thread>
#include <cmath>

#include <syslog.h>

#define NANOSECOND_PER_SECOND 1000000000
#define MICROSECOND_PER_SECOND 1000000
#define MILLISECOND_PER_SECOND 1000

class Clock
{
public:
    //funtions
    Clock(uint32_t period, uint32_t adjust);
    void Start();
    void LogEvent(std::string &input, std::string &token);
    void Tick();
    void Adjust(uint64_t tick_num);
    void ServiceStart();
    void ServiceEnd(std::string &serviceName);
    double Duration_Seconds();

private:
    //functions
    Clock();
    double Duration();
    double TickDuration();
    void test();

    //Members
    std::chrono::time_point<std::chrono::steady_clock> m_StartTime, m_ServiceStart;
    std::chrono::nanoseconds m_Tick, m_Adjustment, m_TickAdjusted;
};

#endif //CLOCK_HPP