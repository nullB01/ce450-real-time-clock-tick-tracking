#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

int main()
{
    Canny();
    VideoCapture vid(0);
    Mat frame;
    namedWindow("capture", CV_WINDOW_AUTOSIZE);

    char c = 'n';
    while(1)
    {
        vid>>frame;
        imshow("capture", frame);
        c = cvWaitKey(50);
        if(c == 'q') break;
    }

    for(int i = 0; i < 100; i++)
    {
        vid>>frame;
        std::stringstream fileName;
        fileName<<"image"<<i<<".ppm";
        imwrite(fileName.str().c_str(), frame);
    }
    
    return 0;
}