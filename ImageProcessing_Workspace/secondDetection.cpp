//Using starter code diff interactive
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define PERCENT_DIFF_THRESHOLD 50.0



int main()
{
    bool eventOccured;
    cv::Mat mat_gray, prev, diff, gaussian;
    std::string path = "../../images/image", file;
    uint32_t maxdiff, diffSum; 
    double percentDiff; 
    
    cv::VideoCapture cap(0);
    cv::namedWindow("diff");

    //file = path + std::to_string(0) + ".ppm";
    //mat_gray = cv::imread(file, CV_LOAD_IMAGE_GRAYSCALE);
    cap>>mat_gray;
    prev = mat_gray.clone();
    diff = mat_gray.clone();

    maxdiff = diff.cols * diff.rows * 255;
    double prevMax = 100;
    for(int i = 1; i < 100000; i++)
    {
        //Get image
        //file = path + std::to_string(i) + ".ppm";
        //mat_gray = cv::imread(file, CV_LOAD_IMAGE_GRAYSCALE);
        cap>>mat_gray;


        //process
        cv::absdiff(mat_gray, prev, diff);

        //Get percent of didferent pixels 
        diffSum = (unsigned int)cv::sum(mat_gray)[0]; //single channel sum
        //percentDiff = ((double)diffSum / (double)maxdiff) * 100.0;
        //std::cout<<std::endl<<percentDiff<<std::endl;

        double min, max;
        minMaxLoc(diff, &min, &max);
        std::cout<<"min: "<<min<<" max: "<<max<<std::endl;
        
        if(max > 1.5 * prevMax)
        {
            //flag event
            eventOccured = true;
            //get timestamp
            std::cout<<"tic"<<std::endl;
        }
        prevMax = max;
        
        
        
        imshow("diff", diff);
        prev = mat_gray.clone();
        char c = cvWaitKey(50);
    }

    return 0;
}