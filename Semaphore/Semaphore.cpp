#include "Semaphore.hpp"

void Semaphore::Wait_Caller()
{
    m_Caller.wait();
}

void Semaphore::Post_Caller()
{  
    m_Caller.post();
}

void Semaphore::Wait_Callee()
{
    m_Callee.wait();
}

void Semaphore::Post_Callee()
{
    m_Callee.post();
}