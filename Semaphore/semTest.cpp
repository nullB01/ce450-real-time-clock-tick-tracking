#include <iostream>
#include <thread>
#include <chrono>
#include <functional>

#include "Semaphore.hpp"

void callee(Semaphore &sem);

int main()
{
    Semaphore sem;

    std::thread calleeThread(callee, std::ref(sem));
    std::cout<<"Caller about to wait"<<std::endl;
    sem.Wait_Caller();
    std::cout<<"Caller released about to sleep for a second then release callee"<<std::endl;
    //std::this_thread::sleep_for(std::chrono::seconds(1)); 
    std::cout<<"Caller post callee"<<std::endl;
    sem.Post_Callee();
    calleeThread.join();
    return 0;
}

void callee(Semaphore &sem)
{
    //std::this_thread::sleep_for(std::chrono::seconds(1)); //wait for lock 
    std::cout<<"Callee released caller about to wait for caller"<<std::endl;
    sem.Post_Caller();
    std::cout<<"Callee about to wait"<<std::endl;
    sem.Wait_Callee();
    std::cout<<"Released by Caller now exiting"<<std::endl;
}