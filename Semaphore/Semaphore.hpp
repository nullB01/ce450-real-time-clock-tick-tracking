/*
* Semaphore.hpp
* Providing semaphore mechanism for tasks / sequencer for startup / dispatch.
* Kevin Pereira
*/

#ifndef SEMAPHORE_HPP
#define SEMAPHORE_HPP

#include <condition_variable>
#include <mutex>

typedef struct basicSem
{
public:
    basicSem()
    {
        m_Lock = std::unique_lock<std::mutex>(m_Mutex);
    }

    void wait()
    {
        m_CV.wait(m_Lock);
    }

    void post()
    {
        m_CV.notify_all();
    }
private:
    std::mutex m_Mutex;
    std::condition_variable m_CV;
    std::unique_lock<std::mutex> m_Lock;
}BasicSem;

class Semaphore
{
public:
    void Wait_Caller();
    void Post_Caller();
    void Wait_Callee();
    void Post_Callee();
private: 
    basicSem m_Caller, m_Callee;
};

#endif //SEMAPHORE_HPP