#include <iostream>
#include <thread>
#include <mutex>
#include <sstream>
#include <string>
#include <iomanip>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "../../Service/Service.hpp"
#include "../../Clock/Clock.hpp"
#include "../../Semaphore/Semaphore.hpp"
#include "../../Video/Video.hpp"

#define NUM_FRAMES 1801
#define DUMP_FRAMES 1
#define LOWERTHRESHOLD 1.45//1.45
#define UPPERTHRESHOLD 2.0
#define FREQUENCY 10

void Sequencer();
void ImageCapture(Semaphore &Sem, Clock &clock);
void Store(Semaphore &Sem, Clock &clock);
void Interpretter(Semaphore &Sem, Clock &clock);

bool g_ContinueFlag = true;
std::mutex g_SaveImageMtx;
Video g_Vid(0);

int main()
{
    //Create Sequencer 
    Service sequencer(Sequencer, 0, 1, 10, 10);

    sequencer.GetThread()->join();

    return 0;
}

void Sequencer()
{
    std::chrono::seconds tickSecond(0);
    std::chrono::nanoseconds tickLength;
    Clock clock(NANOSECOND_PER_SECOND / FREQUENCY, 30000);

    //Create services
    Service Capture_Serv(ImageCapture, 0, 2, 1, 1, clock);
    Capture_Serv.AuxBlock();
    Service Interpretter_Serv(Interpretter, 2, 2, 1, 1, clock);
    Interpretter_Serv.AuxBlock();
    Service Store_Serv(Store, 4, 2, 10, 10, clock);
    Store_Serv.AuxBlock();

    //Logging 
    std::ostringstream os;
    std::string msg("Dispatch"), token("SEQUENCER:");

    clock.Start();
    
    for(int frame = 0; frame < (DUMP_FRAMES + NUM_FRAMES) * FREQUENCY; frame++) //frame can be seen same as tick 
    {
        clock.Tick();
        //Duration tracking
        clock.ServiceStart();
        //Dispatch tracking 
        clock.LogEvent(msg, token);

        if(frame % Capture_Serv.GetInfo().Period == 0)
        {
            Capture_Serv.Unblock();
        }

        if(frame % Interpretter_Serv.GetInfo().Period == 0)
        {
            Interpretter_Serv.Unblock();//And unblock any services there after. 
        }

        if(frame % Store_Serv.GetInfo().Period == 0)
        {
            Store_Serv.Unblock();
        }

        //Adjust clock
        clock.Adjust(frame + 1); 
        //get service runtime trace
        clock.ServiceEnd(token);
    }

    g_ContinueFlag = false;

    Capture_Serv.Unblock();
    Interpretter_Serv.Unblock();
    Store_Serv.Unblock();

    Store_Serv.GetThread()->join();
}

void ImageCapture(Semaphore &Sem, Clock &clock)
{
    int frameNum = 1;
    std::ostringstream os;
    std::string Header("Dispatched,"), Token("IMAGECAPTURE: "), msg;
    Sem.Post_Caller();

    std::cout<<"Capture service started"<<std::endl;

    while(g_ContinueFlag)
    {
        Sem.Wait_Callee();

        //start runtime trace 
        clock.ServiceStart();
        //dispatch Trace
        clock.LogEvent(Header, Token);

        os << "Frame captured," << std::to_string(frameNum) << ',';

        msg = os.str();
        //clock.LogEvent(msg, Token);

        g_Vid.QueryFrame();

        os.str("");
        os.clear();

        //Trace run time 
        clock.ServiceEnd(Token);
    }
}

void Store(Semaphore &Sem, Clock &clock)
{   
    bool falsePositive = false;
    int frameNum = -1;
    std::ostringstream os_SysLog, os_Filename;
    std::string Header("Saved Frame for storage,"), 
                Token("STORE: "), 
                trace("Dispatch,"), 
                msg("Dispatched"), 
                fileName, path("../../images/"),
                FrameNum_str,
                ElapsedTime_str;
    double timestamp;
    std::ostringstream os_Frame, os_ElapsedTime;

    Sem.Post_Caller();

    std::cout<<"Store service started"<<std::endl;

    while(g_ContinueFlag)
    {
        Sem.Wait_Callee();
        
        //Start run time trace 
        clock.ServiceStart();
        //dispatch trace 
        clock.LogEvent(trace, Token);

        os_Filename << path << std::setfill('0') << std::setw(4) << frameNum << ".ppm";
        os_Frame << "Frame " << std::to_string(frameNum) << " of " << std::to_string(NUM_FRAMES - DUMP_FRAMES); FrameNum_str = os_Frame.str();
        os_ElapsedTime << std::to_string(clock.Duration_Seconds()); ElapsedTime_str = os_ElapsedTime.str();
        fileName = os_Filename.str();
        os_SysLog << Header << frameNum << ',';
        msg = os_SysLog.str();

        

        g_SaveImageMtx.lock();

        if(falsePositive)
        {

            clock.LogEvent(msg, Token);
            if(frameNum >= 0)
                g_Vid.SaveFrame(fileName, FrameNum_str, ElapsedTime_str);
            frameNum++;
            os_Filename.str(""); os_Filename.clear();
            os_SysLog.str(""); os_SysLog.clear();
            os_Frame.str(""); os_Frame.clear();
            os_ElapsedTime.str(""); os_ElapsedTime.clear();
        }
        else
        {
            falsePositive = true;
            os_Filename.str("");
            os_Filename.clear();
        }
        
        g_SaveImageMtx.unlock();

        //get service run time
        clock.ServiceEnd(Token);
    }
}

void Interpretter(Semaphore &Sem, Clock &clock)
{
    cv::Mat mat_gray, prev, diff;
    double min, max, ticMaxPrev, max_prev = 30;
    bool ticOccured = false;
    std::string msg("Dispatched"), token("INTERPRETTER"), counts("DIFF_MAX,"), counts_msg;

    //get frame
    g_Vid.QueryFrame();
    g_Vid.GetFrame(prev);
    prev.copyTo(diff);
    
    Sem.Post_Caller();

    std::cout<<"Interpretter service started"<<std::endl;

    while(g_ContinueFlag)
    {
        Sem.Wait_Callee();

        //start run time trace
        clock.ServiceStart();
        //dispatch trace
        clock.LogEvent(msg, token);

        //Get frame
        g_Vid.GetFrame(mat_gray);

        //find max difference between both frames
        cv::absdiff(mat_gray, prev, diff);
        cv::minMaxLoc(diff, &min, &max);
        //std::cout<<"max: "<<max<<std::endl; //@@@
        
        counts_msg = counts + std::to_string(max);
        clock.LogEvent(counts_msg, token);

        if(max > LOWERTHRESHOLD * max_prev )
        {
                //mark event set new max
                ticOccured = true;
                //std::cout<<"tick"<<std::endl;
                ticMaxPrev = max;
        }

        if(ticOccured && max < ticMaxPrev / UPPERTHRESHOLD)
        {
                if(g_SaveImageMtx.try_lock())
                {
                    g_Vid.SaveFrame();
                    g_SaveImageMtx.unlock();
                }
                ticOccured = false;   
        }

        max_prev = max;
        mat_gray.copyTo(prev);

        //get run time
        clock.ServiceEnd(token);
    }

}