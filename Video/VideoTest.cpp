#include "Video.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

int main()
{
    cv::Mat mat_frame;
    cv::namedWindow("capture", CV_WINDOW_AUTOSIZE);
    Video vid(0);

    while(1)
    {
        vid.QueryFrame();
        vid.GetFrame(mat_frame);
        cv::imshow("capture", mat_frame);
        
        char c = cv::waitKey(50);
    }
}