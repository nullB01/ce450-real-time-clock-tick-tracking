#ifndef VIDEO_HPP
#define VIDEO_HPP

#define HRES 1280
#define VRES 720
#define BUFFER_SIZE 10


#include <iostream>
#include <string>

#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h> /*for v4l drivers and applications with public api */
#include <sys/ioctl.h> /*used for device parameters of special files */
#include <sys/mman.h>
#include <unistd.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class Video
{
public:
    enum VID_MODE {V4L, OPENCV};

    Video(int IOPort);
    void QueryFrame();
    void GetFrame(cv::Mat &frame);
    void SaveFrame(std::string &fileName, std::string &frame, std::string &elapsedTime);
    void SaveFrame();
    ~Video();
private: 
    Video(); //block
    void initv4l();
    int m_ReadIndex, m_QueryIndex; //Use two indeces one for query buffer, one for read_index 
    uint16_t m_FrameBuffer[BUFFER_SIZE];
    cv::VideoCapture m_Capture;
    int xioctl(int fd, int request, void *arg);

    cv::Mat m_RingBuffer[BUFFER_SIZE];
    cv::Mat m_SaveFrame, m_gray_frame, m_prev_frame;
    bool m_firstPass;
    double m_min, m_max;
};

#endif //VIDEO_HPP