#include "Video.hpp"

Video::Video(int IOPort):
m_QueryIndex(0),
m_ReadIndex(0),
m_Capture("/dev/video0", cv::CAP_V4L2),
m_firstPass(false)
{
    cv::Mat initFrame;
    //init camera
    for(int i = 0; i < 20; i++)
    {
        m_Capture >> initFrame;
    }
}

void Video::QueryFrame()
{
    cv::Mat frame;
    m_Capture.read(frame);
    frame.copyTo(m_RingBuffer[m_QueryIndex]);
    //cv::cvtColor(m_gray_frame, m_RingBuffer[m_QueryIndex], CV_BGR2GRAY);

    m_RingBuffer[m_QueryIndex].copyTo(m_prev_frame);

    m_QueryIndex++; 
    m_QueryIndex %= BUFFER_SIZE;
    
}

void Video::GetFrame(cv::Mat &frame)
{
    m_RingBuffer[(m_QueryIndex + BUFFER_SIZE - 1) % BUFFER_SIZE].copyTo(frame);
    m_ReadIndex++;
    m_ReadIndex %= BUFFER_SIZE;
}

void Video::SaveFrame(std::string &fileName, std::string &frame, std::string &elapsedTime)
{
    cv::putText(m_SaveFrame, elapsedTime.c_str(), cvPoint(30,30), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);
    cv::putText(m_SaveFrame, frame.c_str(), cvPoint(30,45), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);
    cv::imwrite(fileName, m_SaveFrame);
}

void Video::SaveFrame()
{
    m_RingBuffer[m_ReadIndex].copyTo(m_SaveFrame);
}

Video::~Video()
{
    m_Capture.release();
}

int Video::xioctl(int fd, int request, void *arg)
{
    int r;
        do r = ioctl (fd, request, arg);
        while (-1 == r && EINTR == errno);
        return r;
}

void Video::initv4l()
{
    int returnCode;
    std::stringstream CameraPath("/dev/video0");
    
    //Put IO port index to path then open stream
    //CameraPath<<IOPort;
    returnCode = open(CameraPath.str().c_str(), O_RDWR);

    if(returnCode == -1)
    {
        std::cout<<"Error opening video device... exiting"<<std::endl;
        exit(1);
    }

    std::cout<<"Camera opened successfully. Checking compatibility"<<std::endl;

    //Query capture test if camera is compatible with v4l 
    struct v4l2_capability caps = {};
    if(-1 == xioctl(returnCode, VIDIOC_QUERYCAP, &caps))
    {
        std::cout<<"Error querying camera.. Exiting"<<std::endl;
        exit(1);
    }

    //set camera format 
    struct v4l2_format fmt = {0};
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = HRES;
    fmt.fmt.pix.height = VRES;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;

    std::cout<<"Setting camera format"<<std::endl;

    if(-1 == xioctl(returnCode, VIDIOC_S_FMT, &fmt))
    {
        std::cout<<"Error setting format now exiting"<<std::endl;
        exit(1);
    }
}