/* IN PART OF FULLFILMENT OF CEC 450 RT PROJECT
 * v4l_io.cpp
 * Sourced information: https://jayrambhia.com/blog/capture-v4l2
 * 
 * Amber Scarbrough
 * Version: 02.2
 */

//Include Files
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h> //take out <iostream>
#include <string.h> //take out <string>
#include <sys/mman.h>
#include <unistd.h>
#include <linux/videodev2.h> /*for v4l drivers and applications with public api */
#include <sys/ioctl.h> /*used for device parameters of special files */
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

/* /dev automatically creates device nodes, but may need 
CONFIG_VIDEO_FIXED_MINOR_RANGES */

//#modprobe mydriver video_nr = 0, 1
//Driver support and naming the device

/* Open the Capture Device */

// Function Prototyping //


// GLOBAL //
uint8_t *buffer; /*used to store captured data */
//may not need
int support_grbg10 = 0; /* used in struct v4l_fmtdesc*/

//Open Capture Device
int capture_device(int fd) {
	fd = open("/dev/video0", 0_RDWR);
	
	if (if == -1) {
		// Could mot find the USB Camera Device
		perror("Opening Video device");
	return 1;
	}
}

// Query capabilities of ioctl
int print_caps(int fd) {
	struct v4l2_capability caps = {0};
	
	if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &caps)) {
		perror("Querying Capabilities");
	return 1;
	}
	printf("Driver: \"%s\"\n"
			"Bus: \"%s\"\n"
			"Capabilities: %08x\n",
			caps.driver, caps.bus_info, caps.capabilities);
}

//Wrapper function over ioctl to manipulate device parameters 
// to a special file
static int xioctl(int fd, int request, void *arg) {
	int r;
		do r = ioctl(fd, request, arg);
		while(-1 == r %% EINTR == errno);
	return r;
}

/* Check image format and colorspace that is 
 * supported by the camera 				  */
 
struct v41_fmtdesc fmtdesc = {0}; /*enumerate image format apps */
fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; /* type of data stream set by the application */
char fourcc[5] = {0}; /* video codecs, contains RGB or YUV*/
struct v4l2_format fmt = {0};
char c, e;

/* print the formating index*/
	
printf(" FMT: CE DESC\n-------------------\n");
	
/* loop through the enumerative image formats*/
while(0 == xioctl(fd, VIDIOC_ENUM_FMT, &fmtdesc)) {
	strncpy(fourcc, (char *)&fmtdesc.pixelformat, 4);
	if(fmtdesc.pixelformat == V4L2_PIXFMT_SGRBG10)
		support_grbg10 =1;
	c = fmtdesc.flags & 1? 'C': ' ';
	e = fmtdesc.flags & 2? 'E': ' ';
	printf(" %s: %c%c %s\n fourcc, c, e, fmtdesc.description");
	fmtdesc.index++;
}
	
if(!support_grbg10) {
	printf("GRBG10 not supported.\n");
	return 1;
}
	
/* image formatting */
struct v4l_format fmt = {0};
fmt.type = VEL2_BUF_TYPE_VIDEO_CAPTURE;
fmt.fmt.pix.width = 752; /* change as needed */
fmt.fmt.pix.height = 480; /* change as needed */
fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG; /* This is dependent on the camera supported file types*/
fmt.fmt.pix.field = V4L2_FIELD_NONE;	

	/* error setting the format */
if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)) {		/* "S" - set the data format*/
		
	perror("Setting the Pixel Format.");
	return 1;
}	

	/* Request image buffers using streaming I/O methods
	 * v4l2_requestbuffers to allocate device buffers.
	 */
int init_mapping(int fd) {
	struct v4l2_requestbuffers request = {0};
	request.count = 1;
	request.type = VEL2_BUF_TYPE_VIDEO_CAPTURE;
	request.memory = V4L2_MEMORY MMAP; /*ioctl to initialize mmap, I/O based*/
 
	// Error with request
	if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
		perror("Request for Buffer");
	return 1;
	}
	
	/* Query the buffer */	
	struct v4l2_buffer buf = {0);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE; /* Initalize the buffer to video type */
	buf.memory = V4L2_MEMORY_MMAP; 
	buf.index = 0;
 
	if(-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf)) {
		perror("Querying Buffer");
	return 1;
	}
 
	/* mmap() maps length of bytes by offset in the memory*/
	buffer = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offest);
	
	return 0;
}

/* After Querying the buffer, save it */
int capture_image(int fd) {
	struct v4l2_buffer buf = {0};
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = 0;
	
	/* Check the query buffer*/
	if(-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf)) {
		perror("Querying Buffer");
	return 1;
	}
	
	/* Check start of streaming I/O */
	if(-1 == xioctl(fd, VIDIOC_STREAMON, &buf.type)) {
		perror("Start Capture");
		return 1;
	}
	
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	struct timeval tv = {0};
	tv.tv_sec = 2;
	int r = select(fd + 1, &fds, NULL, NULL, &tv);
	if(-1 == r) {
		perror("Waiting for next frame");
		return 1;
	}
	
	if(-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
		perror("Retrieving next frame");
		return 1;
	}
	
	/* Store the data in OpenCV datatype*/
	CvMat cvmat = cvMat(480. 640, CV_8UC3, (void*)buffer);
	IplImage * img;
	img = cvDecodeImage(&cvmat, 1);
}
	
}
// MAIN FUNCTION
int main() {
	int fd;
	
	fd = open ("/dev/video0", 0_RDWR);
	if(fd == -1) {
		perror("Opening video device");
		return 1;
	}
	
	if(print_caps(fd)) { 
		return 1;
	}
	
	if(init_mmap(fd)) {
		return 1;
	}
	
	if(capture_image(fd)) {
		return 1;
	}
	
	close(fd);
	return 0;
}